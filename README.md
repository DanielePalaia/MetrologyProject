Metrology project for EMC-COE

The project is a spring boot application, it is at the moment composed by 4 packages:

1) MetrologyGenerator: which is implementing the generator, it's based to the algorithm discussed during our first meeting but in multithreading
2) MetrologyClient: it's the client accessing as kafka client and ingesting the gemfire and greenplum database
3) entity-event-bus-service: it's implementing the kafka apis and contains the class which represent the domain model for the scenarios plus some common utility in the various packages
4) MetrologySparkClient: A generator client using Spark Streaming and Spark to consume data from kafka

The project is based on maven so you can install all with:

mvn install

All jars will be made in MetrologyGenerator/target MetrologyClient/target MetrologySparkClient/target

Generator needs configuration files that you can find in conf directory.

Configuration files are needed for the generator (MetrologyGenerator) and are:
generator.properties: to put general configuration about kafka, gemfire and greenplum ip username and passwd and throughput data management
sensorScenarioFields.txt and testScenarioFields.txt: to add further fields to the sensor and test scenarios.

Configuration file is needed for MetrologyClient and MetrologySparkClient:
client.properties: to specify kafka, gemfire, and greenplum configuration
