package  io.pivotal.generator.business.object.eventbus.wafer.random;

import io.pivotal.generator.business.object.eventbus.wafer.TestScenario;

import java.util.Random;
import java.sql.Timestamp;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;

/** Random generation for Test */
public class RandomTest extends TestScenario {
    public RandomTest() {
        // Random generate
        Random rn = new Random();
        // Put at 0 elements dependent on failure probability
        this.setDefectFlag(false);
        this.setAdrId(0);
        this.setAcdId(0);
        this.settypeOfDefectId(0);
        this.setDefectImage("");

        // Create a random timestamp
        String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());

        this.setInspectionTimestamp(timestamp);
        // Set a lot Id
        this.setLotId(rn.nextInt(100) + 1);
        this.setWaferId(rn.nextInt(40) + 1);
        this.setDieId(rn.nextInt(100) + 1);
        this.setReceptId(rn.nextInt(10) + 1);
        this.setMachineId(this.getMachineId());
        this.setTestId(rn.nextInt(100) + 1);
        if(rn.nextInt(2)+1 == 1)  {
            this.setDefectFlag(true);
            this.setAdrId(rn.nextInt(3) + 1);
            this.setAcdId(rn.nextInt(3) + 1);
            this.settypeOfDefectId(rn.nextInt(5) + 1);
            this.setDefectImage("/images/" + this.getLotId() + this.getWaferId() + this.getDieId());
            this.setDefectX("(" + Integer.toString(rn.nextInt(10) + 1) + "," + Integer.toString(rn.nextInt(10) + 1) +")");
        }


    }

    /** Load additional data fields specified in testScenario.txt*/
    public void loadAdditionalFields()  {

        try {
            fieldUtils.loadAdditionalFields("testScenario.txt", this.getAdditionalFields());
        }
        catch (Exception e)  {

        }

    }

    private FieldUtils fieldUtils = new FieldUtils();
}
