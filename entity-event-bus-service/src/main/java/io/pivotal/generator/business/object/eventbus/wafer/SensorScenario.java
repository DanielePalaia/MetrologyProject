package io.pivotal.generator.business.object.eventbus.wafer;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.*;
import java.sql.*;
import java.util.Random;
import org.apache.geode.pdx.PdxSerializable;
import java.util.*;

/**
 * Image data
 */

@JsonPropertyOrder({ "recordTest", "TestTimestamp", "machineId", "sensorId", "result", "valueDescription"})
public class SensorScenario implements Serializable {

    @JsonProperty("recordTest")
    private String recordTest;
    @JsonProperty("TestTimestamp")
    private String timestamp;
    @JsonProperty("machineId")
    private int machineId;
    @JsonProperty("sensorId")
    private int sensorid;
    @JsonProperty("result")
    private int result;
    @JsonProperty("valueDescription")
    private String valueDescription;
    private HashMap<String, String> additionalFields;

  public SensorScenario(String timestamp, int machineId, int sensorId)  {
      this.timestamp = timestamp;
      this.machineId = machineId;
      additionalFields = new HashMap<String, String>();
  }

  public SensorScenario() {

  }

  public void setSensorid(int sensorid) {
    this.sensorid = sensorid;
  }

  public String getTimestamp()  { return timestamp;}

  public int getMachineId()  {return machineId; }

  public int getSensorid() {
    return sensorid;
  }

  public void setRecordTest(String recordTest) {
        this.recordTest = recordTest;
    }

  public String getRecordTest() {
        return recordTest;
    }

  public void setResult(int result) {
      this.result = result;
  }

  public int getResult() {
    return result;
  }
  // Implements equals and hashcode and toString
  /*@Override
  public boolean equals(Object o) {
  }

  @Override
  public int hashCode() {
  }

  @Override
  public String toString() {}*/

    public String getValueDescription()   {
        return valueDescription;
    }

    public void setValueDescription(String valueDescription)   {
        this.valueDescription = valueDescription;
    }

    public HashMap<String, String> getAdditionalFields()  {
        return additionalFields;
    }


}



