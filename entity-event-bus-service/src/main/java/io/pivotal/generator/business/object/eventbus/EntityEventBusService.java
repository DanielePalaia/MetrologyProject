package io.pivotal.generator.business.object.eventbus;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.pivotal.generator.business.object.eventbus.wafer.random.RandomSensor;
import io.pivotal.generator.business.object.eventbus.wafer.random.RandomTest;
import io.pivotal.generator.business.object.eventbus.wafer.random.RandomLog;
import java.util.Properties;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.context.annotation.Configuration;


@Configuration
public class EntityEventBusService {

    private Properties props = null;
    private Producer<Integer, String> producer = null;

    public EntityEventBusService() {
    }

    public EntityEventBusService(String ip, String port) {

        //properties for producer
        System.out.println("ip and port" + ip + port);
        props = new Properties();
        props.put("bootstrap.servers", ip+":"+port);
        props.put("acks", "all");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        //create producer
        producer = new KafkaProducer<Integer, String>(props);
    }


    public void sendTest(RandomTest mytest)   {
        //send messages to my-topic
        ObjectMapper mapper = new ObjectMapper();
        String testScenarioJson = null;
        try {
            testScenarioJson = mapper.writeValueAsString(mytest);
        } catch (Exception e) {
            System.out.println("json unmarshalling error");
            e.printStackTrace();
        }
        ProducerRecord producerRecord = new ProducerRecord<String, String>("mytest", mytest.getInspectionTimestamp(),  testScenarioJson);
        producer.send(producerRecord);

    }

    public void sendSensor(RandomSensor mysensor)   {
        //send messages to my-topic
        ObjectMapper mapper = new ObjectMapper();
        String testSensorScenarioJson = null;
        try {
            testSensorScenarioJson = mapper.writeValueAsString(mysensor);
        } catch (Exception e) {
            System.out.println("json unmarshalling error");
            e.printStackTrace();
        }
        ProducerRecord producerRecord = new ProducerRecord<String, String>("mysensor", mysensor.getTimestamp(),  testSensorScenarioJson);
        producer.send(producerRecord);
    }

    public void sendLog(RandomLog mylog)   {

        ProducerRecord producerRecord = new ProducerRecord<String, String>("mylog", mylog.getTimeStamp(),  mylog.toString());
        producer.send(producerRecord);
    }


}