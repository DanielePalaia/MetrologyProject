package  io.pivotal.generator.business.object.eventbus.wafer.random;

import java.util.*;
import java.io.*;

public class FieldUtils   {

    public static void loadAdditionalFields(String file, HashMap<String, String> inputMap)  {

        Properties testProperties = new Properties();
        try {
            FileInputStream in = new FileInputStream("testScenarioFields.txt");
            testProperties.load(in);
            in.close();
        }
        catch(Exception e)  {
            //e.printStackTrace();
        }

        for(Object key: testProperties.keySet())  {
            String keyElem = (String) key;
            String value = (String) testProperties.get(key);
            String newValue = "";
            Scanner scanValue = new Scanner(value);
            scanValue.useDelimiter(",");
            String scanning = scanValue.next();
            if(!scanValue.hasNext())  {
                System.out.println("Error reading field parameter");
                continue;
            }

            if(scanning.equals("serial")) {
                newValue = scanValue.next();

            }
            else  {
                Random rn = new Random();
                String nextString = scanValue.next();
                /** Case random number by range **/
                if(nextString.indexOf('-') != -1) {
                    Scanner internalScanner = new Scanner(nextString);
                    internalScanner.useDelimiter("-");
                    int first = Integer.parseInt(internalScanner.next());
                    int second = Integer.parseInt(internalScanner.next());
                    newValue = Integer.toString(first + rn.nextInt(second - first + 1));
                    internalScanner.close();
                }
                /** Case random string **/
                else  {
                    ArrayList<String> listElements = new ArrayList<String>();
                    Scanner internalScanner = new Scanner(nextString);
                    internalScanner.useDelimiter("/");
                    while(internalScanner.hasNext())  {
                        listElements.add(internalScanner.next());
                    }

                    newValue = listElements.get(rn.nextInt(listElements.size()));
                    internalScanner.close();
                }
            }

            inputMap.put(keyElem, newValue);
            scanValue.close();

        }

    }

}