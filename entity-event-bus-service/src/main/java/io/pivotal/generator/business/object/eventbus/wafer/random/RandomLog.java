package  io.pivotal.generator.business.object.eventbus.wafer.random;

import  io.pivotal.generator.business.object.eventbus.wafer.LogScenario;
import java.sql.*;

import java.util.Random;

/** Random generation for log scenario if necessary */
public class RandomLog extends LogScenario {
    public RandomLog(String timestamp, int machineId) {
        super(timestamp, machineId);
    }

    public void setRandomValueDescription() {

        Random rn = new Random();
        int res = rn.nextInt(3) +1;
        if(res == 1)  {
            this.setValueDescription("kg/m3");
        }
        else if(res == 2)  {
            this.setValueDescription("µg/m3");
        }
        else  {
            this.setValueDescription("Celsius");
        }
    }

    public void setRandomLog()  {
        Random rn = new Random();
        int res = rn.nextInt(3) +1;

        if(res == 1) {
            this.setCommuniqueType("LOG");
            this.setLog(" Initializing machine: " + this.getMachineId());
        }

        else if(res == 2) {
            this.setCommuniqueType("ERROR");
            this.setLog(" Critical error stopping test ");
        }
        else {
            this.setCommuniqueType("DEBUG");
            this.setLog(" DEBUGGING VALUE Z OF machine_id is " + Integer.toString(res));
        }

    }


}


