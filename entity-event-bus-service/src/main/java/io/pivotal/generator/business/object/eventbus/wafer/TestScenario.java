package  io.pivotal.generator.business.object.eventbus.wafer;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.*;

import java.sql.*;
import java.io.*;

@JsonPropertyOrder({ "recordTest", "inspectionTimestamp", "lotId", "waferId", "reciptId", "machineId",  "testId", "dieId", "defectFlag", "adrId", "acdId", "typeOfDefectId", "defectImage", "defect"})
public class TestScenario implements Serializable {

    public TestScenario()  {
        recordTest = "InspectionRecord";
        machineId = 1;
        additionalFields = new HashMap<String, String>();
    }

    public String getInspectionTimestamp() {
        return inspectionTimestamp;
    }

    public String getRecordTest()  {return recordTest;}

    public void setInspectionTimestamp(String inspectionTimestamp) {
        this.inspectionTimestamp = inspectionTimestamp;
    }

    public int getWaferId() {
        return waferId;
    }

    public void setWaferId(int waferId) {
        this.waferId = waferId;
    }

    public void setDieId(int dieId) {
        this.dieId = dieId;
    }

    public int getDieId() {
        return dieId;
    }

    public void setReceptId(int receptId) {
        this.receptId = receptId;
    }

    public int getReceptId() {
        return receptId;
    }

    public void setMachineId(int machineId) {
        this.machineId = machineId;
    }

    public int getMachineId() {
        return machineId;
    }

    public void setAdrId(int adrId) {
        this.adrId = adrId;
    }

    public int getAdrId() {
        return adrId;
    }

    public void setAcdId(int acdId) {
        this.acdId = acdId;
    }

    public int getAcdId() {
        return acdId;
    }

    public void settypeOfDefectId(int typeOfDefectId) {
        this.typeOfDefectId = typeOfDefectId;
    }

    public int gettypeOfDefectId() {
        return typeOfDefectId;
    }

    public void setDefectX(String defect) {
        this.defect = defect;
    }

    public String getDefect() {
        return defect;
    }

    public int getLotId() {
        return lotId;
    }

    public void setLotId(int lotId) {
        this.lotId = lotId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public int getTestId() {
        return testId;
    }

    public Boolean getDefectFlag()  {
        return defectFlag;
    }

    public void setDefectFlag(Boolean defectFlag)  {
        this.defectFlag = defectFlag;
    }

    public void setDefectImage(String pathFile)   {
        this.defectImage = pathFile;

    }

    public HashMap<String, String> getAdditionalFields()  {
        return additionalFields;
    }

    public String getDefectImage()  {
        return this.defectImage;
    }
    // Implements equals and hashcode and toString
   /* @Override
    public boolean equals(Object o) {
    }

    @Override
    public int hashCode() {
    }

    @Override
    public String toString() {}*/

    @JsonProperty("recordTest")
    private String recordTest;
    @JsonProperty("InspectionTimestamp")
    private String inspectionTimestamp;
    @JsonProperty("waferId")
    private int waferId;
    @JsonProperty("dieId")
    private int dieId;
    @JsonProperty("reciptId")
    private int receptId;
    @JsonProperty("machineId")
    private int machineId;
    @JsonProperty("defectFlag")
    private Boolean defectFlag;
    //@JsonProperty("probDefect")
    //private int probDefect;
    @JsonProperty("adrId")
    private int adrId;
    @JsonProperty("acdId")
    private int acdId;
    @JsonProperty("typeOfDefectId")
    private int typeOfDefectId;
    @JsonProperty("defectImage")
    private String defectImage;
    @JsonProperty("defect")
    private String defect;
    @JsonProperty("lotId")
    private int lotId;
    @JsonProperty("testId")
    private int testId;
    private HashMap<String, String> additionalFields;

}

