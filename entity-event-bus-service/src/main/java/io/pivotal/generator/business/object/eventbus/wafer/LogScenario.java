package io.pivotal.generator.business.object.eventbus.wafer;

import java.util.Random;

/**
 * sensor data
 */
public class LogScenario {

  /**
   * wafer identification
   *
   * @return id
   **/

  public LogScenario(String timestamp, int machineId)  {
    this.timestamp = timestamp;
    this.machineId = machineId;
  }

  public LogScenario()  {

  }

  public String getTimeStamp() {
    return timestamp;
  }

  public void setTimeStamp(String t) {
    this.timestamp = t;
  }

  public String getCommuniqueType() {
    return communiqueType;
  }

  public void setCommuniqueType(String communiqueType) {
    this.communiqueType = communiqueType;
  }

  public int getDieId() {
    return dieId;
  }

  public void setDieId(int dieId) {
    this.dieId = dieId;
  }

  public int getReciptId() {
    return reciptId;
  }

  public void setReciptId(int reciptId) {
    this.reciptId = reciptId;
  }

  public void setTestId(int testId) {
    this.testId = testId;
  }

  public boolean getDefectFlags() {
    return defectFlags;
  }

  public void setDefectFlags(boolean defectFlags) {
    this.defectFlags = defectFlags;
  }

  public int getAcdId() {
    return acdId;
  }

  public void setAdrId(int adrId) {
    this.adrId = adrId;
  }

  public int getAdrId() {
    return adrId;
  }

  public void setAcdId(int acdId) {
    this.acdId = acdId;
  }

  public void setTypeOfDefect(int typeOfDefect) {
    this.typeOfDefect = typeOfDefect;
  }

  public int getTypeOfDefect() {
    return typeOfDefect;
  }

  public void setValueDescription(String valueDescription) {
    this.valueDescription = valueDescription;
  }

  public String getValueDescription() {
    return valueDescription;
  }

  public int getMachineId() {
    return machineId;
  }

  //To String
  public String toString()   {
    return timestamp + " " + communiqueType + " machineID " + machineId + " dieid " + dieId + " recipt id:" + reciptId + log + " " + valueDescription;
  }

  public String getLog()   {
    return log;
  }

  public void setLog(String log)  {
    this.log = log;
  }



  private String timestamp;
  private String communiqueType;
  private int dieId;
  private int reciptId;
  private int machineId;
  private int testId;
  private boolean defectFlags;
  private int adrId;
  private int acdId;
  private int typeOfDefect;
  private String log;
  private String valueDescription;

}


