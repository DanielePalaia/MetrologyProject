package io.pivotal.generator.business.object.eventbus.wafer.random;

import io.pivotal.generator.business.object.eventbus.wafer.SensorScenario;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.sql.*;

/** Random generation for Sensor Scenario if necessary */
public class RandomSensor extends SensorScenario {

    public RandomSensor(String timestamp, int machineId, int sensorId) {
        super(timestamp, machineId,sensorId);
        // Random generate
        Random rn = new Random();
        this.setRecordTest("sensorTest" + Integer.toString(sensorId));
        int res = rn.nextInt(3) +1;
        if(res == 1)  {
            this.setRecordTest("sensorPollution" + Integer.toString(sensorId));
            this.setValueDescription("kg/m3");
        }
        else if(res == 2)  {
            this.setRecordTest("sensorHumidity" + Integer.toString(sensorId));
            this.setValueDescription("µg/m3");
        }
        else  {
            this.setRecordTest("sensorTemperature" + Integer.toString(sensorId));
            this.setValueDescription("Celsius");
        }
    }

    public void setRandomResult()  {
        // Create it randomly
        // Random generate
        Random rn = new Random();
        this.setResult(rn.nextInt(10) +1);

    }

    /** Load additional data fields specified in testScenario.txt*/
    public void loadAdditionalFields()  {

        try {
            fieldUtils.loadAdditionalFields("sensorScenarioFields.txt", this.getAdditionalFields());
        }
        catch(Exception e) {

        }

    }

    private FieldUtils fieldUtils = new FieldUtils();

}
