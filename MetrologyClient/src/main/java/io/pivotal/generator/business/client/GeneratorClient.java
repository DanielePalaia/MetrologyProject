/**
 * Created by mmartofel on 1/31/18.
 */
package io.pivotal.generator.business.client;

import io.pivotal.generator.business.client.gemfireclient.GemFireClient;
import io.pivotal.generator.business.client.gemfireclient.GreenplumClient;
import org.apache.geode.distributed.ServerLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import io.pivotal.generator.business.client.consumers.*;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import java.util.Properties;

import java.io.FileInputStream;

@SpringBootApplication
@EnableTransactionManagement
@EnableConfigurationProperties({ClientProperties.class})
public class GeneratorClient implements CommandLineRunner {

    private Properties clientProperties = new Properties();

    public static void main(String[] args) {
        SpringApplication.run(GeneratorClient.class, args);
    }

    // Here we can run our test as described by e-mail
    @Override
    public void run(String... args) throws Exception {

        FileInputStream in = new FileInputStream("client.properties");
        clientProperties.load(in);

        //Creating a gemfire and greenplum intance
        GemFireClient gemfireClient = new GemFireClient(clientProperties.getProperty("gemfireip"), clientProperties.getProperty("gemfireport"));
        GreenplumClient greenplumClient = new GreenplumClient(clientProperties.getProperty("greenplumurl"), clientProperties.getProperty("greenplumuser"), clientProperties.getProperty("greenplumpasswd"));
        //Inject it to the thread
        kafkaConsumerClientTest clientTest = new kafkaConsumerClientTest(clientProperties.getProperty("kafkaip"), clientProperties.getProperty("kafkaport"), gemfireClient, greenplumClient);
        kafkaConsumerClientSensor clientSensor = new kafkaConsumerClientSensor(clientProperties.getProperty("kafkaip"), clientProperties.getProperty("kafkaport"), gemfireClient, greenplumClient);
        kafkaConsumerClientLog clientLog = new kafkaConsumerClientLog(clientProperties.getProperty("kafkaip"), clientProperties.getProperty("kafkaport"), gemfireClient, greenplumClient);
        Thread threadTestScenario = new Thread(clientTest);
        Thread threadSensorScenario = new Thread(clientSensor);
        Thread threadLogScenario = new Thread(clientLog);
        threadTestScenario.start();
        threadSensorScenario.start();
        threadLogScenario.start();

        try {
            threadTestScenario.join() ;
        }
        catch(Exception e)  {
            System.out.println("error joining thread");
        }



    }

}

