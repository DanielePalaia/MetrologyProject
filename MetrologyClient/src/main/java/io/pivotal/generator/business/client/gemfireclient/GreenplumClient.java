/**
 * Created by Daneile Palaia
 */
package io.pivotal.generator.business.client.gemfireclient;
import org.json.JSONObject;
import java.sql.*;
import io.pivotal.generator.business.object.eventbus.wafer.*;



public class GreenplumClient {

    public Connection connTest = null;
    public Connection connSensor = null;
    public Connection connLog = null;

    public GreenplumClient(String ip, String user, String passwd) {

        try {
            connTest = DriverManager.getConnection(ip, user, passwd);
            connSensor = DriverManager.getConnection(ip, user, passwd);
            connLog = DriverManager.getConnection(ip, user, passwd);
            System.out.println("Connected to the Greenplum successfully.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    public void inserTest(TestScenario testScenario) throws SQLException {
        String insertStatement = "INSERT INTO test_data (testReadID,recordTest,InspectionTimestamp, lotId, waferId, reciptId, machineId, testId, dieId, defectFlag, adrId, acdId, typeOfDefectId, defectImage, defect) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement pstmt = connTest.prepareStatement(insertStatement);
        pstmt.setString(1,testScenario.getInspectionTimestamp());
        pstmt.setString(2,testScenario.getRecordTest());
        pstmt.setString(3,testScenario.getInspectionTimestamp());
        pstmt.setInt(4, testScenario.getLotId());
        pstmt.setInt(5, testScenario.getWaferId());
        pstmt.setInt(6, testScenario.getReceptId());
        pstmt.setInt(7, testScenario.getMachineId());
        pstmt.setInt(8, testScenario.getTestId());
        pstmt.setInt(9, testScenario.getDieId());
        pstmt.setBoolean(10, testScenario.getDefectFlag());
        pstmt.setInt(11, testScenario.getAdrId());
        pstmt.setInt(12, testScenario.getAcdId());
        pstmt.setInt(13, testScenario.gettypeOfDefectId());
        pstmt.setString(14, testScenario.getDefectImage());
        pstmt.setInt(15, 0);
        pstmt.executeUpdate();

    }

    public void insertSensor(SensorScenario sensorScenario) throws SQLException {
        String insertStatement = "INSERT INTO sensor_data (sensorReadID, recordTest, TestTimestamp, machineId, sensorId, result, valueDescription) VALUES (?,?,?,?,?,?,?)";
        PreparedStatement pstmt = connSensor.prepareStatement(insertStatement);
        pstmt.setString(1,sensorScenario.getTimestamp());
        pstmt.setString(2,sensorScenario.getRecordTest());
        pstmt.setString(3,sensorScenario.getTimestamp());
        pstmt.setInt(4, sensorScenario.getMachineId());
        pstmt.setInt(5, sensorScenario.getSensorid());
        pstmt.setInt(6, sensorScenario.getResult());
        pstmt.setString(7, sensorScenario.getValueDescription());
        pstmt.executeUpdate();


    }

    public void insertLog(String key, String log) throws SQLException {
        String insertStatement = "INSERT INTO log_data (logReadID, logLine) VALUES (?,?)";
        PreparedStatement pstmt = connLog.prepareStatement(insertStatement);
        pstmt.setString(1,key);
        pstmt.setString(2,log);
        pstmt.executeUpdate();
    }

}