package io.pivotal.generator.business.client.consumers;

import io.pivotal.generator.business.client.gemfireclient.GreenplumClient;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import java.util.Properties;
import io.pivotal.generator.business.client.gemfireclient.GemFireClient;

public class KafkaConsumerClient  {

    private KafkaConsumer<String, String> consumer = null;
    private GemFireClient gemfireClient = null;
    private GreenplumClient greenplumClient = null;

    public KafkaConsumerClient(String KafkaIp, String KafkaPort, GemFireClient gemfireClient, GreenplumClient greenplumClient) {

        //consumer properties
        Properties props = new Properties();
        props.put("bootstrap.servers", KafkaIp+":"+KafkaPort);
        props.put("group.id", "test-group");

        //using auto commit
        props.put("enable.auto.commit", "true");

        //string inputs and outputs
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        //kafka consumer object
        consumer = new KafkaConsumer<String, String>(props);
        //gemfire client to inject entries
        this.gemfireClient = gemfireClient;
        this.greenplumClient = greenplumClient;


    }

    public KafkaConsumer<String, String> getConsumer()  {
        return consumer;
    }

    public GemFireClient getGemfireClient()   {
        return gemfireClient;
    }

    public GreenplumClient getGreenplumClient() {return greenplumClient;}



}