package io.pivotal.generator.business.client.consumers;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.pivotal.generator.business.client.gemfireclient.GemFireClient;
import io.pivotal.generator.business.client.gemfireclient.GreenplumClient;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import io.pivotal.generator.business.object.eventbus.wafer.*;
import java.util.Arrays;

public class kafkaConsumerClientSensor extends KafkaConsumerClient implements Runnable {

    public kafkaConsumerClientSensor(String KafkaIp, String KafkaPort, GemFireClient gemfireClient, GreenplumClient greenplumClient) {

        super(KafkaIp, KafkaPort, gemfireClient, greenplumClient);
        getConsumer().subscribe(Arrays.asList("mysensor"));

    }

    public void run()    {

        //infinite poll loop
        ObjectMapper mapper = new ObjectMapper();
        while (true) {
            //System.out.println("loopingClientSensor");
            ConsumerRecords<String, String> records = getConsumer().poll(100);
            //System.out.println("after polling ClientSensor");
            try {
                if (records.count() == 0) {
                    Thread.sleep(10);
                }

            }
            catch(Exception e)  {
                e.printStackTrace();
            }

            for (ConsumerRecord<String, String> record : records) {
                SensorScenario obj = null;
                System.out.printf("offset ClientSensor = %d, key = %s, value = %s\n", record.offset(), record.key(), record.value());
                try {
                    obj = mapper.readValue(record.value(), SensorScenario.class);
                }
                catch(Exception e)  {
                    e.printStackTrace();
                }
                getGemfireClient().addSensorEntry(record.key(), record.value());
                try {
                    getGreenplumClient().insertSensor(obj);
                }
                catch(Exception e)  {
                    e.printStackTrace();
                }
            }
        }


    }



}