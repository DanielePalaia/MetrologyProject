package io.pivotal.generator.business.client.consumers;

import io.pivotal.generator.business.client.gemfireclient.GemFireClient;
import io.pivotal.generator.business.client.gemfireclient.GreenplumClient;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import java.util.Arrays;

import java.util.Properties;

public class kafkaConsumerClientLog extends KafkaConsumerClient implements Runnable {

    public kafkaConsumerClientLog(String KafkaIp, String KafkaPort, GemFireClient gemfireClient, GreenplumClient greenplumClient) {

        super(KafkaIp, KafkaPort, gemfireClient, greenplumClient);
        getConsumer().subscribe(Arrays.asList("mylog"));

    }

    public void run()    {

        //infinite poll loop
        while (true) {
            ConsumerRecords<String, String> records = getConsumer().poll(100);
            try {
                if (records.count() == 0) {
                    Thread.sleep(10);
                }

            }
            catch(Exception e)  {
                e.printStackTrace();
            }
            for (ConsumerRecord<String, String> record : records) {
                System.out.printf("offset ClientLog = %d, key = %s, value = %s\n", record.offset(), record.key(), record.value());
                getGemfireClient().addLogEntry(record.key(), record.value());
                try {
                    getGreenplumClient().insertLog(record.key(), record.value());
                }
                catch(Exception e)  {
                    e.printStackTrace();
                }
            }
        }


    }

}