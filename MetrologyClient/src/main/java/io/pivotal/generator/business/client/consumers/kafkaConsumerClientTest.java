package io.pivotal.generator.business.client.consumers;

import io.pivotal.generator.business.client.gemfireclient.GemFireClient;
import io.pivotal.generator.business.client.gemfireclient.GreenplumClient;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import io.pivotal.generator.business.client.consumers.KafkaConsumerClient;
import io.pivotal.generator.business.object.eventbus.wafer.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;

import java.util.Properties;

public class kafkaConsumerClientTest extends KafkaConsumerClient implements Runnable {

    public kafkaConsumerClientTest(String KafkaIp, String KafkaPort, GemFireClient gemfireClient, GreenplumClient greenplumClient) {

        super(KafkaIp, KafkaPort, gemfireClient, greenplumClient);
        getConsumer().subscribe(Arrays.asList("mytest"));

    }

    public void run()    {

        //infinite poll loop
        ObjectMapper mapper = new ObjectMapper();
        while (true) {
            //System.out.println("loopingClientTest");
            ConsumerRecords<String, String> records = getConsumer().poll(100);
            //System.out.println("after pollingClientTest");
            try {
                if (records.count() == 0) {
                    Thread.sleep(10);
                }

            }
            catch(Exception e)  {
                e.printStackTrace();
            }
            for (ConsumerRecord<String, String> record : records) {
                TestScenario obj = null;
                System.out.printf("offsetClientTest = %d, key = %s, value = %s\n", record.offset(), record.key(), record.value());
                try {
                    obj = mapper.readValue(record.value(), TestScenario.class);
                }
                catch(Exception e)  {
                    e.printStackTrace();
                }
                getGemfireClient().addTestEntry(record.key(), record.value());
                try {
                    getGreenplumClient().inserTest(obj);
                }
                catch(Exception e)  {
                    e.printStackTrace();
                }
            }
        }


    }



}