/**
 * Created by mmartofel on 1/31/18.
 */
package io.pivotal.generator.business.client;

import io.pivotal.generator.business.client.gemfireclient.GemFireClient;
import org.apache.geode.distributed.ServerLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import io.pivotal.generator.business.client.consumers.*;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableConfigurationProperties({ClientProperties.class})
public class GeneratorClient implements CommandLineRunner {

    @Autowired
    private ClientProperties properties;

    public static void main(String[] args) {
        SpringApplication.run(GeneratorClient.class, args);
    }

    // Here we can run our test as described by e-mail
    @Override
    public void run(String... args) throws Exception {

        GemFireClient gemfireClient = new GemFireClient(properties.getGemfireIp(), properties.getGemfirePort());
        kafkaConsumerClientTest clientTest = new kafkaConsumerClientTest(properties.getKafkaIp(), properties.getKafkaPort(), gemfireClient);
        kafkaConsumerClientSensor clientSensor = new kafkaConsumerClientSensor(properties.getKafkaIp(), properties.getKafkaPort(), gemfireClient);
        kafkaConsumerClientLog clientLog = new kafkaConsumerClientLog(properties.getKafkaIp(), properties.getKafkaPort(), gemfireClient);
        Thread threadTestScenario = new Thread(clientTest);
        Thread threadSensorScenario = new Thread(clientSensor);
        Thread threadLogScenario = new Thread(clientLog);
        threadTestScenario.start();
        threadSensorScenario.start();
        threadLogScenario.start();
        try {
            threadTestScenario.join() ;
        }
        catch(Exception e)  {
            System.out.println("error joining thread");
        }



    }

}

