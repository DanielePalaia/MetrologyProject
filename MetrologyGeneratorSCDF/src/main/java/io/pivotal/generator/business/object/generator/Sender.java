package io.pivotal.generator.business.object.generator;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.pivotal.generator.business.object.eventbus.wafer.random.RandomLog;
import io.pivotal.generator.business.object.eventbus.wafer.random.RandomSensor;
import io.pivotal.generator.business.object.eventbus.wafer.random.RandomTest;
import java.io.PrintWriter;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.cloud.stream.annotation.*;
import org.springframework.context.MessageSource;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.annotation.*;
import org.springframework.messaging.Message;
import org.springframework.context.annotation.*;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.integration.annotation.*;

@EnableBinding(Source.class)
public class Sender   {

    static public void sendTestScenarioFile(PrintWriter fileTest, RandomTest mytest) {
        //Object to JSON in file
        ObjectMapper mapper = new ObjectMapper();
        try {
            fileTest.println(mapper.writeValueAsString(mytest));
        } catch (Exception e) {
            System.out.println("json error test");
            e.printStackTrace();
        }
    }

    static public void sendSensorScenarioFile(PrintWriter fileSensor, RandomSensor SS) {
        //Object to JSON in file
        ObjectMapper mapper = new ObjectMapper();
        try {
            fileSensor.println(mapper.writeValueAsString(SS));
        } catch (Exception e) {
            System.out.println("json error sensor");
            e.printStackTrace();
        }
    }

    static public void sendLogScenarioFile(PrintWriter fileLog, RandomLog LS) {
        //Object to JSON in file
        try {
            synchronized (fileLog) {
                fileLog.println(LS.toString());
            }
        } catch (Exception e) {
            System.out.println("json error sensor");
            e.printStackTrace();
        }
    }

    static public void sendSCDF(Source source, RandomSensor mysensor) {
        //send messages to my-topic

        ObjectMapper mapper = new ObjectMapper();
        String testSensorScenarioJson = null;
        try {
            testSensorScenarioJson = mapper.writeValueAsString(mysensor);
        } catch (Exception e) {
            System.out.println("json unmarshalling error");
            e.printStackTrace();
        }
        source.output().send(MessageBuilder.withPayload(testSensorScenarioJson).build());


    }

    static public void sendSCDF(Source source, RandomTest mytest) {
        //send messages to my-topic
        ObjectMapper mapper = new ObjectMapper();
        String testTestScenarioJson = null;
        try {
            testTestScenarioJson = mapper.writeValueAsString(mytest);
        } catch (Exception e) {
            System.out.println("json unmarshalling error");
            e.printStackTrace();
        }
        source.output().send(MessageBuilder.withPayload(testTestScenarioJson).build());

    }

    static public void sendSCDF(Source source, RandomLog mylog) {
        //send messages to my-topic
        source.output().send(MessageBuilder.withPayload(mylog.toString()).build());
    }

    static public String sendSCDF(RandomLog mylog) {
        return mylog.toString();
    }

    static public String sendSCDF(String mystring) {
        return mystring;
    }

}
