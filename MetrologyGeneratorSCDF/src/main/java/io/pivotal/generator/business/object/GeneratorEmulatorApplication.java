package io.pivotal.generator.business.object;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import java.io.*;
import org.springframework.boot.CommandLineRunner;
import io.pivotal.generator.business.object.generator.*;
import org.springframework.beans.factory.annotation.Value;
import java.util.Properties;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.cloud.stream.annotation.*;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.annotation.*;
import java.util.*;
import org.springframework.messaging.*;

@EnableBinding(Source.class)
@EnableConfigurationProperties({GeneratorEmulatorProperties.class})
@SpringBootApplication
public class GeneratorEmulatorApplication implements CommandLineRunner {

    @Autowired
    private Source source;
    @Autowired
    private GeneratorEmulatorProperties generatorProperties;

    public static void main(String[] args) {

        SpringApplication.run(GeneratorEmulatorApplication.class, args);
    }

    // Here we can run our test as described by e-mail
    @Override
    public void run(String... args) throws Exception {

        Scenarios testScenario = new Scenarios();

        testScenario.Scenario1(Long.valueOf(Integer.valueOf(generatorProperties.getSensorThroughput())), Integer.valueOf(generatorProperties.getSensorSize()), Integer.valueOf(generatorProperties.getTestThroughput()), Integer.valueOf(generatorProperties.getTestSize()), source);
    }
}
