package io.pivotal.business.object.generator.serializer;

/*import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.pivotal.business.object.model.EntityEvent;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
*/
public class FileSender /*implements EventSender*/ {
    /*private BufferedOutputStream bos;
    private ObjectMapper mapper;
    private String filename;
    private String finalExtension;

    public FileSender(String fileName, String finalExtension) throws IOException {
        this.filename = fileName;
        bos = new BufferedOutputStream(new FileOutputStream(this.filename));
        mapper = new ObjectMapper();
        mapper.getFactory().disable(JsonGenerator.Feature.AUTO_CLOSE_TARGET);
    }

    @Override
    public void sendEvent(EntityEvent event) throws Exception {
        mapper.writeValue(bos, event);
        bos.write(13);
        bos.write(10);
    }

    @Override
    public void close() throws Exception {
        bos.close();
        new File(this.filename).renameTo(new File(this.filename + ".txt"));
    }

    @JsonProperty("fileName")
    public String getFilename() {
        return filename;
    }*/

}
