package io.pivotal.generator.business.object.generator;

import io.pivotal.generator.business.object.eventbus.wafer.random.RandomTest;
import io.pivotal.generator.business.object.eventbus.wafer.random.RandomSensor;
import io.pivotal.generator.business.object.eventbus.wafer.random.RandomLog;
import java.io.*;
import java.util.Date;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.SimpleDateFormat;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import java.util.concurrent.ConcurrentLinkedQueue;
import io.pivotal.generator.business.object.eventbus.EntityEventBusService;
import java.util.Properties;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.cloud.stream.annotation.*;
import org.springframework.messaging.Message;
import org.springframework.context.annotation.*;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.integration.annotation.*;
import org.springframework.integration.support.MessageBuilder;

@EnableBinding(Source.class)
public class Scenarios   {

    public void Scenario1(long sensorThroughout, int sensorSize, long testThroughout, int testSize, Source source) {

        ConcurrentLinkedQueue<RandomTest> globalQueue = new ConcurrentLinkedQueue<RandomTest>();
        // Running main scenario as thread
        MainScenario mainScenario = new MainScenario(sensorThroughout, sensorSize, globalQueue, source);
        LogFileScenario logFileScenario = new LogFileScenario(source);
        SendTest testScenario = new SendTest(globalQueue, testThroughout, testSize, source);
        Thread threadMainScenario = new Thread(mainScenario);
        Thread threadLogFileScenario = new Thread(logFileScenario);
        Thread threadTestScenario = new Thread(testScenario);
        threadMainScenario.start();
        threadLogFileScenario.start();
        threadTestScenario.start();
        try {
            threadMainScenario.join() ;
        }
        catch(Exception e)  {
            System.out.println("error joining thread");
        }

    }
}

// This class implements the main unending scenario
class MainScenario implements Runnable {

    public MainScenario(long throughout, int size, ConcurrentLinkedQueue<RandomTest> globalQueue, Source source)  {
        this.bytesSensor = (((throughout*1000000) / 60)/60);
        this.sizeSensorStruct = size;
        this.numSensorMessagesPerSecond = (int) (this.bytesSensor / sizeSensorStruct);
        this.globalQueue = globalQueue;
        this.source = source;
        System.out.println("Byte sensor per second" + bytesSensor + " Number of message per second:" + String.valueOf(this.numSensorMessagesPerSecond));
    }

    public void run()    {

        System.out.println("GENERATING ENTRIES...");
        // Creating an object mapper to be able to marshall

        int countSensorMessages = 0;

        int lot_id = 1;
        // To make it non-ending
        while (true) {
            // Looping on wafer_id
            for (int w = 1; w <= 40; w++) {

                // Loop over recipt (in some wafer more than one recipt has to be changed
                for (int r = 1; r <= 10; r++) {

                    // Loop over tests
                    for (int t = 1; t <= 100; t++) {
                        long t1Sensor = System.nanoTime();
                        countSensorMessages = doTest(lot_id, w, r, t, countSensorMessages, t1Sensor);
                    }

                }

            }
            lot_id++;
        }
    }

   private int doTest(int lot_id, int w, int r, int t, int countSensorMessages, long t1Sensor)    {

        ObjectMapper mapper = new ObjectMapper();
        // Loop a single test
        for (int d = 1; d <= 100; d++) {
            // Looping over die_id for a single wafer
            RandomTest testScenario = new RandomTest();
            testScenario.setLotId(lot_id);
            testScenario.setWaferId(w);
            testScenario.setDieId(d);
            testScenario.setReceptId(r);
            testScenario.setTestId(t);

            //Store sensor datas
            for (int s = 1; s <= 100; s++) {
                // Filling sensor datas
                RandomSensor sensorScenario = new RandomSensor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()), testScenario.getMachineId(), s);
                sensorScenario.setSensorid(s);
                // random
                sensorScenario.setRandomResult();

                // Log stuff
                RandomLog logScenario = new RandomLog(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()), testScenario.getMachineId());
                logScenario.setCommuniqueType("LOG");
                logScenario.setDieId(testScenario.getDieId());
                logScenario.setReciptId(testScenario.getReceptId());
                logScenario.setTestId(testScenario.getTestId());
                logScenario.setDefectFlags(testScenario.getDefectFlag());
                logScenario.setAdrId(testScenario.getAdrId());
                logScenario.setAcdId(testScenario.getAcdId());
                logScenario.setTypeOfDefect(testScenario.gettypeOfDefectId());
                if (testScenario.getDefectFlag() == true) {
                    logScenario.setLog(" The test for this die failed sensor data: " + sensorScenario.getResult());
                } else {
                    logScenario.setLog(" The test for this die suceeded sensor data: " + sensorScenario.getResult());
                }

                logScenario.setValueDescription(sensorScenario.getValueDescription());
                sensorScenario.loadAdditionalFields();

                Sender.sendSCDF(source, sensorScenario);
                Sender.sendSCDF(source, logScenario);

                countSensorMessages++;
                //System.out.println("mess:" + countSensorMessages);
                long t2 = System.nanoTime();

                //System.out.println("time diff is " +  NANOSECONDS.toMillis((t2 - t1Sensor)) + " count mess " + countSensorMessages + " countSensorMessagesPersec " + numSensorMessagesPerSecond);
                if((NANOSECONDS.toMillis((t2 - t1Sensor)) < 1000 && countSensorMessages > numSensorMessagesPerSecond))  {
                    System.out.println("Generated number of sensor messages:  " + Integer.toString(countSensorMessages));
                    try {
                        //System.out.println("sleeping: " + String.valueOf(1000 - NANOSECONDS.toMillis(t2 - t1Sensor)));
                        Thread.sleep((1000 - NANOSECONDS.toMillis(t2 - t1Sensor)));
                    }  catch (Exception e) {
                    }
                    t1Sensor = System.nanoTime();
                    countSensorMessages = 0;


                }
                else if (countSensorMessages < numSensorMessagesPerSecond) {
                    continue;
                }
                else if(NANOSECONDS.toMillis((t2 - t1Sensor)) > 1000 || countSensorMessages > numSensorMessagesPerSecond)  {
                    System.out.println("Generated number of sensor messages:  " + Integer.toString(countSensorMessages));
                    t1Sensor =  System.nanoTime();;
                    countSensorMessages = 0;
                }

            }

            try {
                testScenario.loadAdditionalFields();
                globalQueue.add(testScenario);
            }
            catch(Exception e) {

            }
        }
        return countSensorMessages;
    }

    // Number of bytes per second in sensor scenario
    private double bytesSensor;
    // Number of test messages to send per second
    private int numTestMessagesPerSecond;
    // Number of sensor messages to send per second
    private int numSensorMessagesPerSecond;
    // size of sensor struct
    private int sizeSensorStruct;
    //size of test struct
    private int sizeTestStruct;
    private ConcurrentLinkedQueue<RandomTest> globalQueue;
    private Source source;

}

// This class implements the log scenario (putting some logs concerning system errors, info ecc in parallel with MainScenario)
class SendTest implements Runnable {

    public SendTest(ConcurrentLinkedQueue<RandomTest> testQueue, long testThroughput, int testSize, Source source) {
        mapper = new ObjectMapper();
        this.testQueue = testQueue;
        this.source = source;
        this.bytesTest = (((testThroughput*1000000) / 60)/60);
        this.sizeTestStruct = testSize;
        this.numTestMessagesPerSecond = (int) (this.bytesTest / sizeTestStruct) + 1;
        System.out.println("Byte test per second" + bytesTest + " Number of message per second:" + String.valueOf(this.numTestMessagesPerSecond));
    }

    public void run() {

        int numMess = 0;
        long t1Test = System.nanoTime();
        while (true) {
            RandomTest mytest = null;
            try {
                mytest = testQueue.poll();
            }
            catch(Exception e)  {
                e.printStackTrace();
            }
            if(mytest == null) continue;

            Sender.sendSCDF(source, mytest);
            numMess++;
            long t2 = System.nanoTime();

            //System.out.println("time diff is " +  NANOSECONDS.toMillis((t2 - t1Sensor)) + " count mess " + countSensorMessages + " countSensorMessagesPersec " + numSensorMessagesPerSecond);
            if((NANOSECONDS.toMillis((t2 - t1Test)) < 1000 && numMess >= numTestMessagesPerSecond))  {
                try {
                    System.out.println("Generated number of test messages:  " + Integer.toString(numMess));
                    //System.out.println("sleeping: " + String.valueOf(1000 - NANOSECONDS.toMillis(t2 - t1Test)));
                    Thread.sleep((1000 - NANOSECONDS.toMillis(t2 - t1Test)));
                }  catch (Exception e) {
                }
                t1Test = System.nanoTime();
                numMess = 0;


            }
            else if (numMess < numTestMessagesPerSecond) {
                continue;
            }
            else if(NANOSECONDS.toMillis((t2 - numMess)) > 1000 || numMess >= numTestMessagesPerSecond)  {
                System.out.println("Generated number of test messages:  " + Integer.toString(numMess));
                t1Test =  System.nanoTime();;
                numMess = 0;
            }


        }
    }

    private ObjectMapper mapper;
    private double bytesTest;
    private int sizeTestStruct;
    private int numTestMessagesPerSecond;
    ConcurrentLinkedQueue<RandomTest> testQueue;
    private Source source;
}

// This class implements the log scenario (putting some logs concerning system errors, info ecc in parallel with MainScenario)
class LogFileScenario implements Runnable {

    public LogFileScenario(Source source) {

        this.fileLog = fileLog;
        this.source = source;
    }

    public void run() {

        System.nanoTime();

        while (true) {
            RandomTest mytest = new RandomTest();
            RandomLog logScenario = new RandomLog(mytest.getInspectionTimestamp(), mytest.getMachineId());
            logScenario.setRandomLog();

            //kafkaBusService.sendLog(logScenario);
            Sender.sendSCDF(source, logScenario);
            try {
                Thread.sleep(1000);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

        private PrintWriter fileLog = null;
        private Source source;
}