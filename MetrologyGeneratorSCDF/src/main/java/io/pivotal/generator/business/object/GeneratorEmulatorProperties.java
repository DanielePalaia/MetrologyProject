package io.pivotal.generator.business.object;

import java.util.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@ConfigurationProperties("pivotal.generator")
@Validated
public class GeneratorEmulatorProperties    {

    private String sensorThroughput = "5000";
    private String sensorSize = "140";
    private String testThroughput = "1";
    private String testSize = "300";

    public String getSensorThroughput()  {
        return sensorThroughput;
    }

    public String getSensorSize()   {
        return sensorSize;
    }

    public String getTestThroughput()   {
        return testThroughput;
    }

    public String getTestSize()   {
        return testSize;
    }

    public void setSensorThroughput(String sensorThroughput)  {
        this.sensorThroughput = sensorThroughput;
    }

    public void setSensorSize(String sensorSize)   {
        this.sensorSize = sensorSize;
    }

    public void setTestThroughput(String testThroughput)   {
        this.testThroughput = testThroughput;
    }

    public void setTestSize(String testSize)   {
        this.testSize = testSize;
    }

}