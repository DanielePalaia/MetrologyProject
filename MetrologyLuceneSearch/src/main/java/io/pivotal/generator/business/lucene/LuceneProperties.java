package io.pivotal.generator.business.lucene;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

/**
 * @author Daniele Palaia
 */
@ConfigurationProperties("pivotal.generator.client")
@Validated
public class LuceneProperties {

    private String gemfireIp="";
    private String gemfirePort="";

    public String getGemfireIp()  {
        return gemfireIp;
    }

    public String getGemfirePort()  {
        return gemfirePort;
    }

}