/**
 * Created by Daniele Palaia on 1/31/18.
 */
package io.pivotal.generator.business.lucene;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import java.util.Properties;
import java.util.*;

import java.io.FileInputStream;

@SpringBootApplication
@EnableTransactionManagement
public class LuceneSearch {

    public static void main(String[] args) {
        SpringApplication.run(LuceneSearch.class, args);
    }
}

