/**
 * Created by Daniele Palaia.
 */
package io.pivotal.generator.business.lucene.lucenesearch;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

@Controller
@RequestMapping("/lucene")
public class MetrologyLuceneSearchController  {

    @Autowired
    private MetrologyLuceneSearch metrologyLuceneSearch;

    String inputPage="<h2>Lucene search on Gemfire example</h2>\n" +
            "<p style=\"font-size: 1.5em;\">Type some text to search in some logs stored in a gemfire region using Lucene indexes</p>\n" +
            "<table class=\"editorDemoTable\">\n" +
            "<tbody>\n" +
            "  <form action = \"http://localhost:7001/lucene\" method = \"get\">\n" +
            "<tr>\n" +
            "<td>text to search: <input name=\"text\" type=\"text\" /></td>\n" +
            "<td><center><input type=\"submit\" /></center></td>\n" +
             "</tr>\n" +
            "</form>\n" +
            "</tbody>\n" +
            "</table>";

    @RequestMapping(method=RequestMethod.GET)
    public @ResponseBody String searchPresentation(@RequestParam(value="text", required=false) String text) {
        System.out.println("name: " + text);
        String result = "";
        String tmp="";
        if(text != null) {
            result = metrologyLuceneSearch.query(text);

        }
        tmp = "<ul>\n ";

        String returnOutputPage = inputPage + tmp + result + "</ul>";

        return returnOutputPage;
    }


}
