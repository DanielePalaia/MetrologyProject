/**
 * Created by Daniele Palaia.
 */
package io.pivotal.generator.business.lucene.lucenesearch;

import java.util.Map;

import org.apache.geode.cache.*;
import org.apache.geode.cache.client.*;
import org.apache.geode.cache.Region;
import org.apache.geode.cache.client.ClientRegionShortcut;
import org.apache.geode.distributed.ServerLauncher;
import org.apache.lucene.search.Query;
import java.util.*;
import org.apache.geode.cache.lucene.*;
import java.io.*;
import org.springframework.stereotype.Component;


@Component("MetrologyLuceneSearch")
public class MetrologyLuceneSearch  {

    private LuceneService luceneService;
    private Region<String, String> regionTest;
    private Region<String, String> regionSensor;
    private Region<String, String> regionLog;
    private ClientCache cache;

    public MetrologyLuceneSearch() {
        Properties clientProperties = new Properties();
        try {
            FileInputStream in = new FileInputStream("lucene.properties");
            clientProperties.load(in);
        }
        catch(Exception e)  {
            e.printStackTrace();
        }
        gemfireGetCache(clientProperties.getProperty("gemfireip"), clientProperties.getProperty("gemfireport"));
        luceneService = LuceneServiceProvider.get(cache);

    }

    public String query(String text) {

        String result = "";
        try {
            LuceneQuery query = luceneService.createLuceneQueryFactory()
                    .create("LogLuceneIndex", "mylog", text, "__REGION_VALUE_FIELD");

            PageableLuceneQueryResults<String, String> results = query.findPages();

            //System.out.println("size: " + results.size());
            while (results.hasNext()) {
                System.out.println("looping page:");
                List<LuceneResultStruct<String, String>> resultListElement = results.next();
                for (LuceneResultStruct<String, String> lR : resultListElement) {
                    System.out.println("looping element:");
                    System.out.println("key element found: " + lR.getKey() + " value element Found: " + lR.getValue());
                    result += "<li>" + lR.getValue() + "</li>";
                }
            }
        }

        catch(Exception e)  {
            e.printStackTrace();
        }
        return result;

    }


   public void gemfireGetCache(String ip, String port)  {

            System.out.println("Attempting to start cache server");


            cache = new ClientCacheFactory()
                    .addPoolLocator(ip, Integer.valueOf(port))
                    .create();

            regionLog = cache
                    .<String, String>createClientRegionFactory(ClientRegionShortcut.CACHING_PROXY)
                    .create("mylog");

    }

}
