/**
 * Created by Daniele Palaia.
 */
package io.pivotal.generator.business.client.gemfireclient;

import java.util.Map;

import org.apache.geode.cache.*;
import org.apache.geode.distributed.ServerLauncher;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
public class GemFireClient {

        private Region<String, String> regionTest;
        private Region<String, String> regionSensor;
        private Region<String, String> regionLog;
        private Cache cache;
        private ServerLauncher serverLauncher;

        public GemFireClient(String ip, String port) {

            System.out.println("Attempting to start cache server");

            this.serverLauncher  = new ServerLauncher.Builder()
                .setMemberName("myserver")
                .set("locators","localhost[10334]")
                //.set("cache-xml-file", "cache.xml")
                .set("log-level", "info")
                .build();
            serverLauncher.start();
            System.out.println("gemfire server successfully started");

            PartitionAttributesFactory paf = new PartitionAttributesFactory()
                    .setTotalNumBuckets(3);

            cache = new CacheFactory()
                    .create();

            cache.createRegionFactory(RegionShortcut.PARTITION)
                    .setPartitionAttributes(paf.create())
                    .create("mytest");

            regionTest = cache.getRegion("mytest");


            cache.createRegionFactory(RegionShortcut.LOCAL)
                    .create("mysensor");

            regionSensor = cache.getRegion("mysensor");

            cache.createRegionFactory(RegionShortcut.LOCAL)
                    .create("mylog");

            regionLog = cache.getRegion("mylog");

            System.out.println("gemfire regions created");

        }

        public void addTestEntry(String key, String value)   {
            regionTest.put(key, value);
            //regionTest.put("2", "World");

            /*for (Map.Entry<String, String>  entry : region.entrySet()) {
                System.out.format("key = %s, value = %s\n", entry.getKey(), entry.getValue());

            cache.close();*/
        }

        public void addSensorEntry(String key, String value)   {
            regionSensor.put(key, value);
            //regionTest.put("2", "World");

            /*for (Map.Entry<String, String>  entry : region.entrySet()) {
                System.out.format("key = %s, value = %s\n", entry.getKey(), entry.getValue());

            cache.close();*/
        }

        public void addLogEntry(String key, String value)   {
            regionLog.put(key, value);
            //regionTest.put("2", "World");

            /*for (Map.Entry<String, String>  entry : region.entrySet()) {
                System.out.format("key = %s, value = %s\n", entry.getKey(), entry.getValue());

            cache.close();*/
    }



}
