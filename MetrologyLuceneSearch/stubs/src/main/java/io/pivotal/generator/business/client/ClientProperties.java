package io.pivotal.generator.business.client;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

/**
 * @author Daniele Palaia
 */
@ConfigurationProperties("pivotal.generator.client")
@Validated
public class ClientProperties {

    private String kafkaip="52.174.100.94";
    private String kafkaport="9092";
    private String gemfireIp="127.0.0.1";
    private String gemfirePort="10334";


    public String getKafkaIp()  {
        return kafkaip;
    }

    public String getKafkaPort()  {
        return kafkaport;
    }

    public String getGemfireIp()  {
        return gemfireIp;
    }

    public String getGemfirePort()  {
        return gemfirePort;
    }



}