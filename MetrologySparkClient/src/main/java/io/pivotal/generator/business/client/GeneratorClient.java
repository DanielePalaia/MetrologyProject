/**
 * Created by mmartofel on 1/31/18.
 */
package io.pivotal.generator.business.client;

import org.apache.spark.streaming.Durations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import java.util.Properties;
import java.util.*;
import io.pivotal.generator.business.client.gemfireclient.*;
import io.pivotal.generator.business.client.consumers.*;
import org.apache.spark.SparkConf;

import java.io.FileInputStream;

@SpringBootApplication
@EnableTransactionManagement
@EnableConfigurationProperties({ClientProperties.class})
public class GeneratorClient implements CommandLineRunner {

    private Properties clientProperties = new Properties();

    public static void main(String[] args) {
        SpringApplication.run(GeneratorClient.class, args);
    }

    // Here we can run our test as described by e-mail
    @Override
    public void run(String... args) throws Exception {

        SparkConf conf = new SparkConf().setMaster("local[8]").setAppName("NetworkWordCount");
        FileInputStream in = new FileInputStream("client.properties");
        clientProperties.load(in);

        //Creating a gemfire and greenplum intance
        GemFireClient gemfireClient = new GemFireClient(clientProperties.getProperty("gemfireip"), clientProperties.getProperty("gemfireport"));
        GreenplumClient greenplumClient = new GreenplumClient(clientProperties.getProperty("greenplumurl"), clientProperties.getProperty("greenplumuser"), clientProperties.getProperty("greenplumpasswd"));
        //Inject it to the thread
        kafkaSparkConsumer sparkKafkaConsumer = new kafkaSparkConsumer(clientProperties.getProperty("kafkaip"), clientProperties.getProperty("kafkaport"), gemfireClient, greenplumClient, conf);
        Thread threadKafkaSparkConsumer = new Thread(sparkKafkaConsumer);
        threadKafkaSparkConsumer.start();

        try {
            threadKafkaSparkConsumer.join() ;
        }
        catch(Exception e)  {
            System.out.println("error joining thread");
        }

    }

}

