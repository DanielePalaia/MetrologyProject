package io.pivotal.generator.business.client.consumers;

import io.pivotal.generator.business.client.gemfireclient.GemFireClient;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import io.pivotal.generator.business.client.consumers.KafkaConsumerClient;
import java.util.Arrays;

import java.util.Properties;

public class kafkaConsumerClientTest extends KafkaConsumerClient implements Runnable {

    public kafkaConsumerClientTest(String KafkaIp, String KafkaPort, GemFireClient gemfireClient) {

        super(KafkaIp, KafkaPort, gemfireClient);
        getConsumer().subscribe(Arrays.asList("mytest"));

    }

    public void run()    {

        //infinite poll loop
        while (true) {
            System.out.println("loopingClientTest");
            ConsumerRecords<String, String> records = getConsumer().poll(100);
            System.out.println("after pollingClientTest");
            try {
                if (records.count() == 0) {
                    Thread.sleep(10);
                }

            }
            catch(Exception e)  {
                e.printStackTrace();
            }
            for (ConsumerRecord<String, String> record : records) {
                System.out.printf("offsetClientTest = %d, key = %s, value = %s\n", record.offset(), record.key(), record.value());
                getGemfireClient().addTestEntry(record.key(), record.value());
            }
        }


    }



}