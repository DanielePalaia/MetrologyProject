/**
 * Created by daniele palaia on 1/31/18.
 */
package io.pivotal.generator.business.client;

import org.apache.spark.streaming.Durations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import java.util.Properties;
import java.util.*;
import io.pivotal.generator.business.client.gemfireclient.*;
import io.pivotal.generator.business.client.consumers.*;
import org.apache.spark.SparkConf;

import java.io.FileInputStream;

@SpringBootApplication
public class GeneratorClient  {

    public static void main(String[] args) {
        SpringApplication.run(GeneratorClient.class, args);
    }


}

