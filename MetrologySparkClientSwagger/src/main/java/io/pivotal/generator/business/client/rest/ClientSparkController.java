package io.pivotal.generator.business.client.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import java.io.*;
import io.pivotal.generator.business.client.gemfireclient.*;
import io.pivotal.generator.business.client.consumers.*;
import org.apache.spark.SparkConf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

/**
 * Created by daniele palaia.
 */
@RestController
public class ClientSparkController {

    Thread threadMainScenario = null;
    /** 0 NOT CREATED/DELETED
     *  1 RUNNING
     */

    int actualValue = 0;

    @RequestMapping(value = "/spark", method = RequestMethod.POST,  params = {"kafkaip", "kafkaport", "gemfireip", "gemfireport", "greenplumurl", "greenplumusername", "greenplumupasswd"})
    public ResponseEntity<Object>  createClientSpark(@RequestParam(value="kafkaip", defaultValue="localhost") String kafkaip, @RequestParam(value="kafkaport", defaultValue="9092") String kafkaport,
                                                   @RequestParam(value="gemfireip", defaultValue="localhost") String gemfireIp, @RequestParam(value="gemfireport", defaultValue="10334") String gemfirePort, @RequestParam(value="greenplumurl", defaultValue="jdbc:postgresql://localhost:5432/metrology") String greenplumUrl,
                                                   @RequestParam("greenplumusername") String greenplumUser, @RequestParam("greenplumupasswd") String greenplumPasswd) {


        runThread(kafkaip, kafkaport, gemfireIp, gemfirePort, greenplumUrl, greenplumUser, greenplumPasswd);
        actualValue = 1;

        return ResponseEntity.ok("Client Spark Started!");

    }

    /*@RequestMapping(value = "/spark", method = RequestMethod.PUT)
    public ResponseEntity<Object>  modifyClientSpark() {

        if(actualValue == 1)  {
            return ResponseEntity.ok("Spark Client already up");
        }
        threadMainScenario.resume();
        actualValue = 1;
        return ResponseEntity.ok("Client Spark Started!");

    }

    @RequestMapping(value = "/spark", method = RequestMethod.DELETE)
    public ResponseEntity<Object>  deleteClientSpark() {

        if(threadMainScenario == null)  {
            return ResponseEntity.ok("Spark Client already up");
        }

        actualValue = 0;
        threadMainScenario.suspend();

        return ResponseEntity.ok("client Spark Interrupted!");

    }*/

    public void runThread(String kafkaIp, String kafkaPort, String gemfireIp, String gemfirePort, String greenplumUrl, String greenplumUsername, String greenplumPasswd)  {
        SparkConf conf = new SparkConf().setMaster("local[8]").setAppName("NetworkWordCount");

        try {
            //Creating a gemfire and greenplum intance
            GemFireClient gemfireClient = new GemFireClient(gemfireIp, gemfirePort);
            GreenplumClient greenplumClient = new GreenplumClient(greenplumUrl, greenplumUsername, greenplumPasswd);
            //Inject it to the thread
            kafkaSparkConsumer sparkKafkaConsumer = new kafkaSparkConsumer(kafkaIp, kafkaPort, gemfireClient, greenplumClient, conf);
            threadMainScenario = new Thread(sparkKafkaConsumer);
            threadMainScenario.start();
        }

        catch(Exception e)  {
            e.printStackTrace();
        }
    }
}