/**
 * Created by Daniele Palaia.
 */
package io.pivotal.generator.business.client.gemfireclient;

import java.util.Map;

import org.apache.geode.cache.*;
import org.apache.geode.cache.client.*;
import org.apache.geode.cache.Region;
import org.apache.geode.cache.client.ClientRegionShortcut;
import org.apache.geode.distributed.ServerLauncher;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import io.pivotal.generator.business.object.eventbus.wafer.*;
import org.apache.commons.lang3.RandomStringUtils;
import java.util.UUID;
import java.io.Serializable;
import java.util.*;


@EnableTransactionManagement
public class GemFireClient implements Serializable {

    private Region<String, String> regionTest;
    private Region<String, String> regionSensor;
    private Region<String, String> regionLog;
    private ClientCache cache;
    private ServerLauncher serverLauncher;

    public GemFireClient(String ip, String port) {

        System.out.println("Attempting to start cache server");


        cache = new ClientCacheFactory()
                .addPoolLocator(ip, Integer.valueOf(port))
                .create();

        regionTest = cache
                .<String, String>createClientRegionFactory(ClientRegionShortcut.CACHING_PROXY)
                .create("mytest");

        regionSensor = cache
                .<String, String>createClientRegionFactory(ClientRegionShortcut.CACHING_PROXY)
                .create("mysensor");

        regionLog = cache
                .<String, String>createClientRegionFactory(ClientRegionShortcut.CACHING_PROXY)
                .create("mylog");

        System.out.println("gemfire regions created");

    }

    public void addTestEntry(String value)   {
        UUID uuid = UUID.randomUUID();
        String key = uuid.toString();
        regionTest.put(key, value);
    }

    public void addSensorEntry(String value)   {
        UUID uuid = UUID.randomUUID();
        String key = uuid.toString();
        regionSensor.put(key, value);
    }

    public void addLogEntry(String value)   {

        UUID uuid = UUID.randomUUID();
        String key = uuid.toString();
        regionLog.put(key, value);
    }

    public void addSensorEntries(ArrayList<String> gemfireSensorList)   {
        HashMap<String, String> gemfireSensorMap = new HashMap<String, String>();

        for(String line: gemfireSensorList) {
            UUID uuid = UUID.randomUUID();
            String key = uuid.toString();
            gemfireSensorMap.put(key, line);
        }
        regionSensor.putAll(gemfireSensorMap);
    }

    public void addTestEntries(ArrayList<String> gemfireTestList)   {
        HashMap<String, String> gemfireTestMap = new HashMap<String, String>();

        for(String line: gemfireTestList) {
            UUID uuid = UUID.randomUUID();
            String key = uuid.toString();
            gemfireTestMap.put(key, line);
        }
        regionTest.putAll(gemfireTestMap);
    }

    public void addLogEntries(ArrayList<String> gemfireLogList)   {
        HashMap<String, String> gemfireLogMap = new HashMap<String, String>();

        for(String line: gemfireLogList) {
            UUID uuid = UUID.randomUUID();
            String key = uuid.toString();
            gemfireLogMap.put(key, line);
        }
        regionLog.putAll(gemfireLogMap);
    }

}
