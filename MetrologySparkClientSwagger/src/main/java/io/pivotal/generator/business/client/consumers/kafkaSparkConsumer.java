package io.pivotal.generator.business.client.consumers;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.pivotal.generator.business.client.gemfireclient.GemFireClient;
import io.pivotal.generator.business.client.gemfireclient.GreenplumClient;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import io.pivotal.generator.business.object.eventbus.wafer.*;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.*;
import org.apache.spark.SparkConf;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import java.time.*;
import org.apache.spark.streaming.Durations;
import java.io.*;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.api.java.JavaRDD;

import java.util.*;

public class kafkaSparkConsumer implements Serializable, Runnable {

    private KafkaConsumer<String,String > consumer = null;
    private GemFireClient gemfireClient = null;
    private GreenplumClient greenplumClient = null;
    private JavaStreamingContext jssc;
    private Map<String, Object> kafkaParams;

    public kafkaSparkConsumer(String KafkaIp, String KafkaPort, GemFireClient gemFireClient,  GreenplumClient greenplumClient, SparkConf conf) throws Exception {

        this.jssc = new JavaStreamingContext(conf, Durations.seconds(1));

        kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers", KafkaIp+":"+KafkaPort);
        kafkaParams.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringDeserializer");
        kafkaParams.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringDeserializer");
        kafkaParams.put(ConsumerConfig.GROUP_ID_CONFIG,"group1");
        kafkaParams.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"latest");
        kafkaParams.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,true);

        this.gemfireClient = gemFireClient;
        this.greenplumClient = greenplumClient;

    }

    public JavaDStream<String> addChannel(String topic)  {

        // Start point of spark computation reading from kafka
        JavaInputDStream<ConsumerRecord<String, String>> messages = KafkaUtils.createDirectStream(
                jssc,
                LocationStrategies.PreferConsistent(),
                ConsumerStrategies.Subscribe(Arrays.asList(topic), kafkaParams)
        );

        // Take just the value part of the mssage with a map op
        JavaDStream<String> lines = messages.map(ConsumerRecord::value);
        ObjectMapper mapper = new ObjectMapper();

        // Take all the lines
        try {
            lines.foreachRDD(new VoidFunction<JavaRDD<String>>() {

                public void call(JavaRDD<String> rdd) throws Exception {
                ArrayList<String> linesTest = new ArrayList<String>();
                ArrayList<String> linesSensor = new ArrayList<String>();
                ArrayList<String> linesLog = new ArrayList<String>();

                System.out.println("--- New RDD with " + rdd.partitions().size()
                        + " partitions and " + rdd.count() + " records");

                for(String line: rdd.collect())  {
                    if(topic.equals("mytest")) {
                        linesTest.add(line);
                    }

                    else if(topic.equals("mysensor")) {
                        linesSensor.add(line);
                    }

                    else {
                        linesLog.add(line);
                    }

                }

                // Store in gemfire
                gemfireClient.addSensorEntries(linesSensor);
                gemfireClient.addTestEntries(linesTest);
                gemfireClient.addLogEntries(linesLog);
                try {
                    // Store in greenplum
                    greenplumClient.insertTests(linesTest);
                    greenplumClient.insertSensors(linesSensor);
                    greenplumClient.insertLogs(linesLog);
                }
                catch (Exception e)  {
                    e.printStackTrace();
                }

                }});

        }
        catch (Exception e)  {
            e.printStackTrace();
        }

        return lines;

    }

    /** Run the spark thread */
    public void run()   {

        System.out.println("running");

        // Add 3 channels
        JavaDStream<String> testStream = addChannel("mytest");
        JavaDStream<String> sensorStream = addChannel("mysensor");
        JavaDStream<String> logStream = addChannel("mylog");

        // Start spark computation
        try {
            jssc.start();
            jssc.awaitTermination();
        }
        catch(Exception e)  {
            e.printStackTrace();
        }

    }

}