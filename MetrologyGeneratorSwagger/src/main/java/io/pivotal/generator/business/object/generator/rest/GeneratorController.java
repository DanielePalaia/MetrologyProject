package io.pivotal.generator.business.object.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import io.pivotal.generator.business.object.generator.Scenarios;
import java.util.*;
import java.io.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

/**
 * Created by daniele palaia.
 */
@RestController
public class GeneratorController {

    Thread threadMainScenario = null;
    /** 0 NOT CREATED/DELETED
     *  1 CREATED/RUNNING
     *  2 SUSPENDED
     *  3 RUNNING */

    int actualValue = 0;
    Scenarios scenario = null;

    @RequestMapping(value = "/generator", method = RequestMethod.POST,  params = {"kafkaip", "kafkaport", "sensorThroughput", "sensorSize", "testThroughput", "testSize"})
    public ResponseEntity<Object>  createGenerator(@RequestParam(value="kafkaip", defaultValue="localhost") String kafkaip, @RequestParam(value="kafkaport", defaultValue="9092") String kafkaport,
                             @RequestParam(value="sensorThroughput", defaultValue="5000") String sensorThroughput, @RequestParam(value="sensorSize", defaultValue="145") String sensorSize, @RequestParam(value="testThroughput", defaultValue="1") String testThroughput,
                             @RequestParam(value="testSize",defaultValue="305") String testSize) {


        System.out.println("Sensor throughput per hour: " + sensorThroughput);
        System.out.println("Size of message: " + sensorSize);
        System.out.println("Test throughput per hour: " + testThroughput);
        System.out.println("Size of message: " + testSize);

        scenario = new Scenarios(Long.valueOf(sensorThroughput), Integer.valueOf(sensorSize), Long.valueOf(testThroughput), Integer.valueOf(testSize), kafkaip, kafkaport);

        if(threadMainScenario != null)   {
            return ResponseEntity.ok("Generator already created!");
        }
        actualValue = 1;
        threadMainScenario = new Thread(scenario);
        threadMainScenario.start();

        return ResponseEntity.ok("Generator Started!");

    }

    @RequestMapping(value = "/generator", method = RequestMethod.PUT)
    public ResponseEntity<Object>  restartGenerator() {

        if(actualValue != 2)  {
            return ResponseEntity.ok("Generator already running!");
        }
        actualValue = 3;
        scenario.resumes();

        return ResponseEntity.ok("Generator resumed!");

    }

    @RequestMapping(value = "/generator", method = RequestMethod.DELETE)
    public ResponseEntity<Object>  suspendGenerator() {

        if(actualValue == 2)  {
            return ResponseEntity.ok("Generator already suspended!");
        }

        actualValue = 2;
        scenario.suspends();

        return ResponseEntity.ok("Generator Suspended!");

    }

    @RequestMapping(value = "/generator", method = RequestMethod.GET)
    public ResponseEntity<Object>  getGeneratorInfo() {

        if(threadMainScenario == null)   {
            return ResponseEntity.ok("Generator not yet started, start it first with a POST request");

        }

        long sensorMessagesMade = scenario.getSensorMessagesMade();
        long testMessagesMade = scenario.getTestMessagesMade();
        long logMessagesMade = scenario.getLogMessagesMade();

        double sensorDataGenerated = (sensorMessagesMade * scenario.getSensorSize()) / 1000000;
        double testDataGenerated = (testMessagesMade * scenario.getTestSize()) / 1000000;
        double logDataGenerated = (logMessagesMade * scenario.getLogSize()) / 1000000;

        return ResponseEntity.ok("STATISTICS: NUMBER OF MESSAGES AND DATA PRODUCED TILL NOW: \n SensorMessagesMade: " + sensorMessagesMade + "\n DataSensorGenerated (MB): " + sensorDataGenerated + "\n TestMessagesMade: " + testMessagesMade + "\n TestDataGenerated (MB): " + testDataGenerated+ "\n LogMessagesMade: " + logMessagesMade + "\n LogDataGenerated (MB): " + logDataGenerated);

    }
}