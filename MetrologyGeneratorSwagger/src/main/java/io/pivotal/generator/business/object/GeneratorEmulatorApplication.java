package io.pivotal.generator.business.object;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import java.io.*;
import org.springframework.boot.CommandLineRunner;
import io.pivotal.generator.business.object.generator.Scenarios;
import org.springframework.beans.factory.annotation.Value;
import java.util.Properties;


@SpringBootApplication
public class GeneratorEmulatorApplication {

    Properties generatorProperties = new Properties();

    public static void main(String[] args) {
        SpringApplication.run(GeneratorEmulatorApplication.class, args);
    }

}
