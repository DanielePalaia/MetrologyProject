package io.pivotal.generator.business.object.generator;

import io.pivotal.generator.business.object.eventbus.wafer.random.RandomTest;
import io.pivotal.generator.business.object.eventbus.wafer.random.RandomSensor;
import io.pivotal.generator.business.object.eventbus.wafer.random.RandomLog;
import java.io.*;
import java.util.Date;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.SimpleDateFormat;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import java.util.concurrent.ConcurrentLinkedQueue;
import io.pivotal.generator.business.object.eventbus.EntityEventBusService;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;


public class Scenarios implements Runnable   {

    public Scenarios(long sensorThroughout, int sensorSize, long testThroughout, int testSize, String kafkaIp, String kafkaPort)  {

        this.sensorThroughout = sensorThroughout;
        this.sensorSize = sensorSize;
        this.testThroughout = testThroughout;
        this.testSize = testSize;
        this.kafkaPort = kafkaPort;
        this.kafkaIp = kafkaIp;
        this.numberOfMessagesSensor = new AtomicLong(0);
        this.numberOfMessagesTest = new AtomicLong(0);
        this.numberOfMessagesLog = new AtomicLong(0);

    }

    public void run() {

        EntityEventBusService kafkaBusService = new EntityEventBusService(kafkaIp, kafkaPort) ;
        ConcurrentLinkedQueue<RandomTest> globalQueue = new ConcurrentLinkedQueue<RandomTest>();
        // Running main scenario as thread
        MainScenario mainScenario = new MainScenario(sensorThroughout, sensorSize, globalQueue, kafkaBusService, this.numberOfMessagesSensor);
        LogFileScenario logFileScenario = new LogFileScenario(kafkaBusService, this.numberOfMessagesLog);
        SendTest testScenario = new SendTest(globalQueue, testThroughout, testSize, kafkaBusService, numberOfMessagesTest);
        threadMainScenario = new Thread(mainScenario);
        threadLogFileScenario = new Thread(logFileScenario);
        threadTestScenario = new Thread(testScenario);
        threadMainScenario.start();
        threadLogFileScenario.start();
        threadTestScenario.start();
        try {
            threadMainScenario.join() ;
        }
        catch(Exception e)  {
            System.out.println("error joining thread");
        }

    }

    public void suspends()  {
        threadMainScenario.suspend();
        threadLogFileScenario.suspend();
        threadTestScenario.suspend();
    }

    public void resumes()  {
        threadMainScenario.resume();
        threadLogFileScenario.resume();
        threadTestScenario.resume();
    }

    public long getSensorMessagesMade()  {
        return numberOfMessagesSensor.longValue();
    }

    public long getTestMessagesMade()  {

        return numberOfMessagesTest.longValue();
    }

    public long getLogMessagesMade()  {
        return numberOfMessagesSensor.longValue() + numberOfMessagesLog.longValue();

    }

    public long getSensorSize()  {
        return sensorSize;
    }

    public long getTestSize()  {

        return testSize;
    }

    public long getLogSize()  {
        return 100;

    }

    private long sensorThroughout;
    private int sensorSize;
    private long testThroughout;
    private int testSize;
    private String kafkaIp;
    private String kafkaPort;
    private Thread threadMainScenario;
    private Thread threadLogFileScenario;
    private Thread threadTestScenario;
    private AtomicLong numberOfMessagesSensor;
    private AtomicLong numberOfMessagesTest;
    private AtomicLong numberOfMessagesLog;
}

// This class implements the main unending scenario
class MainScenario implements Runnable {

    public MainScenario(long throughout, int size, ConcurrentLinkedQueue<RandomTest> globalQueue, EntityEventBusService kafkaBusService, AtomicLong numberOfMessagesSensor)  {
        this.bytesSensor = (((throughout*1000000) / 60)/60);
        this.sizeSensorStruct = size;
        this.numSensorMessagesPerSecond = (int) (this.bytesSensor / sizeSensorStruct);
        this.globalQueue = globalQueue;
        this.kafkaBusService = kafkaBusService;
        this.numberOfMessagesSensor = numberOfMessagesSensor;
        System.out.println("Byte sensor per second" + bytesSensor + " Number of message per second:" + String.valueOf(this.numSensorMessagesPerSecond));
    }

    public void run()    {

        System.out.println("GENERATING ENTRIES...");
        // Creating an object mapper to be able to marshall

        int countSensorMessages = 0;

        int lot_id = 1;
        // To make it non-ending
        while (true) {
            // Looping on wafer_id
            for (int w = 1; w <= 40; w++) {

                // Loop over recipt (in some wafer more than one recipt has to be changed
                for (int r = 1; r <= 10; r++) {

                    // Loop over tests
                    for (int t = 1; t <= 100; t++) {
                        long t1Sensor = System.nanoTime();
                        countSensorMessages = doTest(lot_id, w, r, t, countSensorMessages, t1Sensor);
                    }

                }

            }
            lot_id++;
        }
    }

   private int doTest(int lot_id, int w, int r, int t, int countSensorMessages, long t1Sensor)    {

        ObjectMapper mapper = new ObjectMapper();
        // Loop a single test
        for (int d = 1; d <= 100; d++) {
            // Looping over die_id for a single wafer
            RandomTest testScenario = new RandomTest();
            testScenario.setLotId(lot_id);
            testScenario.setWaferId(w);
            testScenario.setDieId(d);
            testScenario.setReceptId(r);
            testScenario.setTestId(t);

            //Store sensor datas
            for (int s = 1; s <= 100; s++) {
                // Filling sensor datas
                RandomSensor sensorScenario = new RandomSensor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()), testScenario.getMachineId(), s);
                sensorScenario.setSensorid(s);
                // random
                sensorScenario.setRandomResult();

                // Log stuff
                RandomLog logScenario = new RandomLog(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()), testScenario.getMachineId());
                logScenario.setCommuniqueType("LOG");
                logScenario.setDieId(testScenario.getDieId());
                logScenario.setReciptId(testScenario.getReceptId());
                logScenario.setTestId(testScenario.getTestId());
                logScenario.setDefectFlags(testScenario.getDefectFlag());
                logScenario.setAdrId(testScenario.getAdrId());
                logScenario.setAcdId(testScenario.getAcdId());
                logScenario.setTypeOfDefect(testScenario.gettypeOfDefectId());
                if (testScenario.getDefectFlag() == true) {
                    logScenario.setLog(" The test for this die failed sensor data: " + sensorScenario.getResult());
                } else {
                    logScenario.setLog(" The test for this die suceeded sensor data: " + sensorScenario.getResult());
                }

                logScenario.setValueDescription(sensorScenario.getValueDescription());
                sensorScenario.loadAdditionalFields();

                kafkaBusService.sendSensor(sensorScenario);
                kafkaBusService.sendLog(logScenario);

                countSensorMessages++;
                //System.out.println("mess:" + countSensorMessages);
                long t2 = System.nanoTime();

                //System.out.println("time diff is " +  NANOSECONDS.toMillis((t2 - t1Sensor)) + " count mess " + countSensorMessages + " countSensorMessagesPersec " + numSensorMessagesPerSecond);
                if((NANOSECONDS.toMillis((t2 - t1Sensor)) < 1000 && countSensorMessages > numSensorMessagesPerSecond))  {
                    System.out.println("Generated number of sensor messages:  " + Integer.toString(countSensorMessages));
                    try {
                        //System.out.println("sleeping: " + String.valueOf(1000 - NANOSECONDS.toMillis(t2 - t1Sensor)));
                        Thread.sleep((1000 - NANOSECONDS.toMillis(t2 - t1Sensor)));
                    }  catch (Exception e) {
                    }
                    t1Sensor = System.nanoTime();
                    numberOfMessagesSensor.set(numberOfMessagesSensor.longValue() + countSensorMessages);
                    countSensorMessages = 0;

                }
                else if (countSensorMessages < numSensorMessagesPerSecond) {
                    continue;
                }
                else if(NANOSECONDS.toMillis((t2 - t1Sensor)) > 1000 || countSensorMessages > numSensorMessagesPerSecond)  {
                    System.out.println("Generated number of sensor messages:  " + Integer.toString(countSensorMessages));
                    t1Sensor =  System.nanoTime();
                    numberOfMessagesSensor.set(numberOfMessagesSensor.longValue() + countSensorMessages);
                    countSensorMessages = 0;
                }

            }

            try {
                testScenario.loadAdditionalFields();
                globalQueue.add(testScenario);
            }
            catch(Exception e) {

            }
        }
        return countSensorMessages;
    }

    // Number of bytes per second in sensor scenario
    private double bytesSensor;
    // Number of test messages to send per second
    private int numTestMessagesPerSecond;
    // Number of sensor messages to send per second
    private int numSensorMessagesPerSecond;
    // size of sensor struct
    private int sizeSensorStruct;
    //size of test struct
    private int sizeTestStruct;
    private ConcurrentLinkedQueue<RandomTest> globalQueue;
    // Kafka environment
    private EntityEventBusService kafkaBusService;
    AtomicLong numberOfMessagesSensor;

}

// This class implements the log scenario (putting some logs concerning system errors, info ecc in parallel with MainScenario)
class SendTest implements Runnable {

    public SendTest(ConcurrentLinkedQueue<RandomTest> testQueue, long testThroughput, int testSize, EntityEventBusService kafkaBusService, AtomicLong numberOfMessagesTest) {
        mapper = new ObjectMapper();
        this.testQueue = testQueue;
        this.bytesTest = (((testThroughput*1000000) / 60)/60);
        this.sizeTestStruct = testSize;
        this.numTestMessagesPerSecond = (int) (this.bytesTest / sizeTestStruct) + 1;
        this.kafkaBusService = kafkaBusService;
        this.numberOfMessagesTest = numberOfMessagesTest;
        System.out.println("Byte test per second" + bytesTest + " Number of message per second:" + String.valueOf(this.numTestMessagesPerSecond));
    }

    public void run() {

        int numMess = 0;
        long t1Test = System.nanoTime();
        while (true) {
            RandomTest mytest = null;
            try {
                mytest = testQueue.poll();
            }
            catch(Exception e)  {
                e.printStackTrace();
            }
            if(mytest == null) continue;

            /**Use kafka now */
            kafkaBusService.sendTest(mytest);
            numMess++;
            long t2 = System.nanoTime();

            //System.out.println("time diff is " +  NANOSECONDS.toMillis((t2 - t1Sensor)) + " count mess " + countSensorMessages + " countSensorMessagesPersec " + numSensorMessagesPerSecond);
            if((NANOSECONDS.toMillis((t2 - t1Test)) < 1000 && numMess >= numTestMessagesPerSecond))  {
                try {
                    System.out.println("Generated number of test messages:  " + Integer.toString(numMess));
                    //System.out.println("sleeping: " + String.valueOf(1000 - NANOSECONDS.toMillis(t2 - t1Test)));
                    Thread.sleep((1000 - NANOSECONDS.toMillis(t2 - t1Test)));
                }  catch (Exception e) {
                }
                t1Test = System.nanoTime();
                numberOfMessagesTest.set(numberOfMessagesTest.longValue() + numMess);
                numMess = 0;


            }
            else if (numMess < numTestMessagesPerSecond) {
                continue;
            }
            else if(NANOSECONDS.toMillis((t2 - numMess)) > 1000 || numMess >= numTestMessagesPerSecond)  {
                System.out.println("Generated number of test messages:  " + Integer.toString(numMess));
                t1Test =  System.nanoTime();
                numberOfMessagesTest.set(numberOfMessagesTest.longValue() + numMess);
                numMess = 0;
            }


        }
    }

    private ObjectMapper mapper;
    private double bytesTest;
    private int sizeTestStruct;
    private int numTestMessagesPerSecond;
    ConcurrentLinkedQueue<RandomTest> testQueue;
    private EntityEventBusService kafkaBusService;
    private AtomicLong numberOfMessagesTest;
}

// This class implements the log scenario (putting some logs concerning system errors, info ecc in parallel with MainScenario)
class LogFileScenario implements Runnable {

    public LogFileScenario(EntityEventBusService kafkaBusService, AtomicLong numberOfMessagesLog) {

        this.fileLog = fileLog;
        this.kafkaBusService = kafkaBusService;
        this.numberOfMessagesLog = numberOfMessagesLog;
    }

    public void run() {

        System.nanoTime();

        while (true) {
            RandomTest mytest = new RandomTest();
            RandomLog logScenario = new RandomLog(mytest.getInspectionTimestamp(), mytest.getMachineId());
            logScenario.setRandomLog();
            numberOfMessagesLog.set(numberOfMessagesLog.longValue() + 1);

            kafkaBusService.sendLog(logScenario);
            try {
                Thread.sleep(1000);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

        private PrintWriter fileLog = null;
        private  EntityEventBusService kafkaBusService;
        AtomicLong numberOfMessagesLog;
}