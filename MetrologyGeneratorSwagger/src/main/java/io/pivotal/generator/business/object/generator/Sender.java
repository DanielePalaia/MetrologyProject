package io.pivotal.generator.business.object.generator;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.pivotal.generator.business.object.eventbus.wafer.random.RandomLog;
import io.pivotal.generator.business.object.eventbus.wafer.random.RandomSensor;
import io.pivotal.generator.business.object.eventbus.wafer.random.RandomTest;
import java.io.PrintWriter;

public class Sender   {


    static public void sendTestScenarioFile(PrintWriter fileTest, RandomTest mytest) {
        //Object to JSON in file
        ObjectMapper mapper = new ObjectMapper();
        try {
            fileTest.println(mapper.writeValueAsString(mytest));
        } catch (Exception e) {
            System.out.println("json error test");
            e.printStackTrace();
        }
    }

    static public void sendSensorScenarioFile(PrintWriter fileSensor, RandomSensor SS) {
        //Object to JSON in file
        ObjectMapper mapper = new ObjectMapper();
        try {
            fileSensor.println(mapper.writeValueAsString(SS));
        } catch (Exception e) {
            System.out.println("json error sensor");
            e.printStackTrace();
        }
    }

    static public void sendLogScenarioFile(PrintWriter fileLog, RandomLog LS) {
        //Object to JSON in file
        try {
            synchronized (fileLog) {
                fileLog.println(LS.toString());
            }
        } catch (Exception e) {
            System.out.println("json error sensor");
            e.printStackTrace();
        }
    }

}
