package io.pivotal.generator.business.object;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import java.io.*;
import org.springframework.boot.CommandLineRunner;
import io.pivotal.generator.business.object.generator.Scenarios;
import org.springframework.beans.factory.annotation.Value;
import java.util.Properties;


@SpringBootApplication
public class GeneratorEmulatorApplication implements CommandLineRunner {

    Properties generatorProperties = new Properties();

    public static void main(String[] args) {
        SpringApplication.run(GeneratorEmulatorApplication.class, args);
    }

    // Here we can run our test as described by e-mail
    @Override
    public void run(String... args) throws Exception {

        Scenarios testScenario = new Scenarios();
        FileInputStream in = new FileInputStream("generator.properties");
        generatorProperties.load(in);

        System.out.println("Sensor throughput per hour: " + generatorProperties.getProperty("sensor.throughput"));
        System.out.println("Size of message: " + generatorProperties.getProperty("sensor.size"));
        System.out.println("Test throughput per hour: " + generatorProperties.getProperty("test.throughput"));
        System.out.println("Size of message: " +generatorProperties.getProperty("test.size"));
        testScenario.Scenario1(Long.valueOf(generatorProperties.getProperty("sensor.throughput")), Integer.valueOf(generatorProperties.getProperty("sensor.size")), Long.valueOf(generatorProperties.getProperty("test.throughput")), Integer.valueOf(generatorProperties.getProperty("test.size")), generatorProperties.getProperty("kafkaip"), generatorProperties.getProperty("kafkaport"));

    }


}
