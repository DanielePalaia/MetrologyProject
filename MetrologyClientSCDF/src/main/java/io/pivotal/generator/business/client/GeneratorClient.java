/**
 * Created by mmartofel on 1/31/18.
 */
package io.pivotal.generator.business.client;

import io.pivotal.generator.business.client.gemfireclient.GemFireClient;
import io.pivotal.generator.business.client.gemfireclient.GreenplumClient;
import org.apache.geode.distributed.ServerLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import io.pivotal.generator.business.client.consumers.*;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import java.util.Properties;
import org.springframework.cloud.stream.messaging.*;
import org.springframework.cloud.stream.annotation.*;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.annotation.*;
import java.util.*;
import org.springframework.messaging.*;
import java.io.FileInputStream;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@EnableConfigurationProperties({ClientProperties.class})
@EnableBinding(Sink.class)
public class GeneratorClient {

    @Autowired
    private ClientProperties clientProperties;

    private int cont = 0;

    private GemFireClient gemfireClient;

    public static void main(String[] args) {


        SpringApplication.run(GeneratorClient.class, args);
    }


    @StreamListener(Sink.INPUT)
    public void message(String message) {

        if(cont == 0)  {
            gemfireClient =  new GemFireClient(clientProperties.getGemfireIp(), clientProperties.getGemfirePort());
            cont++;
        }

        System.out.println("received {}" + message);
        if (message.contains("InspectionRecord"))  {
            gemfireClient.addTestEntry(message);
        }
        else if(message.contains("sensorTemperature") || message.contains("sensorHumidity") || message.contains("sensorPollution"))  {
            gemfireClient.addSensorEntry(message);
        }
        else if(message.contains("LOG") || message.contains("DEBUG") || message.contains("ERROR"))  {
            gemfireClient.addLogEntry(message);
        }
    }

}

