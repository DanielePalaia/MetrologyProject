/**
 * Created by Daniele Palaia.
 */
/**
 * Created by Daniele Palaia.
 */
package io.pivotal.generator.business.client.gemfireclient;

import java.util.Map;

import org.apache.geode.cache.*;
import org.apache.geode.cache.client.*;
import org.apache.geode.cache.Region;
import org.apache.geode.cache.client.ClientRegionShortcut;
import org.apache.geode.distributed.ServerLauncher;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import io.pivotal.generator.business.object.eventbus.wafer.*;
import org.apache.commons.lang3.RandomStringUtils;
import java.util.UUID;
import java.io.Serializable;
import java.util.*;


@EnableTransactionManagement
public class GemFireClient implements Serializable {

    private Region<String, String> regionTest;
    private Region<String, String> regionSensor;
    private Region<String, String> regionLog;
    private ClientCache cache;
    private ServerLauncher serverLauncher;

    public GemFireClient(String ip, String port) {

        System.out.println("Attempting to start cache server");


        cache = new ClientCacheFactory()
                .addPoolLocator(ip, Integer.valueOf(port))
                .create();

        regionTest = cache
                .<String, String>createClientRegionFactory(ClientRegionShortcut.CACHING_PROXY)
                .create("mytest");

        regionSensor = cache
                .<String, String>createClientRegionFactory(ClientRegionShortcut.CACHING_PROXY)
                .create("mysensor");

        regionLog = cache
                .<String, String>createClientRegionFactory(ClientRegionShortcut.CACHING_PROXY)
                .create("mylog");

        System.out.println("gemfire regions created");

    }

    public void addTestEntry(String value)   {
        UUID uuid = UUID.randomUUID();
        String key = uuid.toString();
        regionTest.put(key, value);
    }

    public void addSensorEntry(String value)   {
        UUID uuid = UUID.randomUUID();
        String key = uuid.toString();
        regionSensor.put(key, value);
    }

    public void addLogEntry(String value)   {

        UUID uuid = UUID.randomUUID();
        String key = uuid.toString();
        regionLog.put(key, value);
    }

}