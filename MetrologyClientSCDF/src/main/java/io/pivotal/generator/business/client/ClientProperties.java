package io.pivotal.generator.business.client;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

/**
 * @author Daniele Palaia
 */
@ConfigurationProperties("pivotal.generator.client")
@Validated
public class ClientProperties {

    private String gemfireIp="127.0.0.1";
    private String gemfirePort="10334";
    private String greenplumUrl="jdbc:postgresql://fca.westeurope.cloudapp.azure.com/gpadmin";
    private String greenplumUser="gpadmin";
    private String greenplumPasswd="changeme";

    public ClientProperties()  {

    }


    public String getGemfireIp()  {
        return gemfireIp;
    }

    public String getGemfirePort()  {
        return gemfirePort;
    }

    public String getGreenPlumIp() { return greenplumUrl;}

    public String getGreenplumUser() { return greenplumUser;}

    public String getGreenplumPasswd() {return greenplumPasswd;}

    public void setGemfireIp(String gemfireIp)  {
        this.gemfireIp = gemfireIp;
    }

    public void setGemfirePort(String gemfirePort)  {
        this.gemfirePort = gemfirePort;
    }

    public void getGreenPlumUrl(String greenplumUrl) {
        this.greenplumUrl = greenplumUrl;
    }

    public void getGreenplumUser(String greenplumUser) {
        this.greenplumUser = greenplumUser;
    }

    public void getGreenplumPasswd(String greenplumPasswd) {
        this.greenplumPasswd = greenplumPasswd;
    }
}