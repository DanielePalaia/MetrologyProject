package io.pivotal.generator.business.client.consumers;

import io.pivotal.generator.business.client.gemfireclient.GreenplumClient;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import java.util.Properties;
import io.pivotal.generator.business.client.gemfireclient.GemFireClient;

public class KafkaConsumerClient  {

    private GemFireClient gemfireClient = null;
    private GreenplumClient greenplumClient = null;

    public KafkaConsumerClient(GemFireClient gemfireClient, GreenplumClient greenplumClient) {

        //gemfire client to inject entries
        this.gemfireClient = gemfireClient;
        this.greenplumClient = greenplumClient;


    }

    public GemFireClient getGemfireClient()   {
        return gemfireClient;
    }

    public GreenplumClient getGreenplumClient() {return greenplumClient;}



}