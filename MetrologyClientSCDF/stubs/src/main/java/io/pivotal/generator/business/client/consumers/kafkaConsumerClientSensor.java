package io.pivotal.generator.business.client.consumers;

import io.pivotal.generator.business.client.gemfireclient.GemFireClient;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import io.pivotal.generator.business.client.consumers.KafkaConsumerClient;
import java.util.Arrays;

import java.util.Properties;

public class kafkaConsumerClientSensor extends KafkaConsumerClient implements Runnable {

    public kafkaConsumerClientSensor(String KafkaIp, String KafkaPort, GemFireClient gemfireClient) {

        super(KafkaIp, KafkaPort, gemfireClient);
        getConsumer().subscribe(Arrays.asList("mysensor"));

    }

    public void run()    {

        //infinite poll loop
        while (true) {
            System.out.println("loopingClientSensor");
            ConsumerRecords<String, String> records = getConsumer().poll(100);
            System.out.println("after polling ClientSensor");
            try {
                if (records.count() == 0) {
                    Thread.sleep(10);
                }

            }
            catch(Exception e)  {
                e.printStackTrace();
            }

            for (ConsumerRecord<String, String> record : records) {
                System.out.printf("offset ClientSensor = %d, key = %s, value = %s\n", record.offset(), record.key(), record.value());
                getGemfireClient().addSensorEntry(record.key(), record.value());
            }
        }


    }



}